
# BostaTask

- Task for Bosta Company for IOS Development Role.
Building App to show random user as entry point for the app with this random user application can generate the user's albums in a nice tableView list, once user touch any of showen album, app takes user to the next screen which is collection of album's photos. In this screen user can search for photo title and app will automatically update the photos list to match user's query.

-  As Bounce, Task done by adding Hero transition photo when user tap photo it opens with nice animation and modern transition in close it by pannig or swipe.
    adding PULL TO REFRECH feature on album's list so it will show new random user's data.
    adding error message with moadern animation and showing picture of Bosta with error message.
    adding APPICON for Bosta Co.
- App built with Combine framework to give the app reactive programming state management 

## Frameworks & Open-source libraries

- UIKit
- Combine
- SnapKit
- Moya
- Kingfisher
- Hero

## Architecture
    - MVVM architecture for better data presisting and decoupling layers.
    - Factory Pattern
    - Clean Architecture approach 
    - Clean Architecture approach 

```
cd existing_repo
git remote add origin https://gitlab.com/ALi_Alaa/bostatask.git
git branch -M main
git push -uf origin main
```
