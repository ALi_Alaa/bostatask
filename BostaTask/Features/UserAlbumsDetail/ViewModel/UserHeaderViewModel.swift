//
//  UserHeaderViewModel.swift
//  UserHeaderViewModel
//
//  Created by MacBook Pro on 16/06/2022.
//

import Foundation

struct UserHeaderViewModel {
    let userName: String
    let address: String
}
