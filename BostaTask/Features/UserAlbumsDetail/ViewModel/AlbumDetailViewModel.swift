//
//  AlbumDetailViewModel.swift
//  AlbumDetailViewModel
//
//  Created by MacBook Pro on 16/06/2022.
//

import Foundation

struct AlbumDetailViewModel{
    let albumId: Int
    let albumTitle: String
}
