//
//  UserAlbumFactory.swift
//  UserAlbumFactory
//
//  Created by MacBook Pro on 14/06/2022.
//

import Foundation

//MARK: - Production State
enum UserAlbumNetworkingState {
    case production
}

//MARK: - User's Album ViewModel Provider
class UserAlbumsViewModelProvider {
    
    //MARK: - View Model Provider function for upcomming networking changes
    func viewModelCalling(state: UserAlbumNetworkingState) -> UserAlbumViewModel  {
        switch state {
        case .production:
            return UserAlbumViewModel(userServiceCaller: UserDetailNetworkSource(), albumServiceCaller: AlbumsDetailNetworkSource())
        }
    }
}
