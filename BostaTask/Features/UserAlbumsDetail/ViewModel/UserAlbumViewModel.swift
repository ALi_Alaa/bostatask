//
//  UserAlbumViewModel.swift
//  UserAlbumViewModel
//
//  Created by MacBook Pro on 13/06/2022.
//

import Foundation
import Combine

class UserAlbumViewModel {
    
    //MARK: - Properties
    private let apiService: UserDetailNetworkProtocol
    
    private let albumApiService: AlbumDetailNetworkProtocol
    
    private var cancellable = Set<AnyCancellable>()
    
    var albums = CurrentValueSubject<[AlbumModel]?, Never>([AlbumModel]())
    
    var userHeaderViewModel = PassthroughSubject<UserHeaderViewModel, Never>()
    var albumTableViewModel = PassthroughSubject<[AlbumDetailViewModel], Never>()
    
    var isloading = CurrentValueSubject<Bool, Never>(false)
    lazy var alertMessage = PassthroughSubject<String, Never>()
    
    //MARK: - init to inject the depenedency
    init(userServiceCaller: UserDetailNetworkProtocol, albumServiceCaller: AlbumDetailNetworkProtocol) {
        self.apiService = userServiceCaller
        self.albumApiService = albumServiceCaller
        fetchUser()
    }
    
    //MARK: - fetching User
    func fetchUser() {
        isloading.send(true)
        apiService.fetchUser()
            .map(UserModel.self)
            .sink { [unowned self] response in
                switch response {
                case .failure(let error):
                    self.isloading.send(false)
                    self.alertMessage.send( error.localizedDescription )
                default:
                    break
                }
            } receiveValue: { [unowned self] data in
                guard let userID = data.id else {
                    self.isloading.send(false)
                    self.alertMessage.send(FAILURE_IN_GETING_USER_DATA)
                    return
                }
                createUserHeaderViewModel(user: data)
                fetchUserAlbums(userId: userID)
            }
            .store(in: &cancellable)
    }
    
    //MARK: - Generate user's header view model
    fileprivate func createUserHeaderViewModel(user: UserModel) {
        guard let userName = user.userName, let userAddress = user.address?.description.description else {return}
        let userHeader: UserHeaderViewModel = UserHeaderViewModel(userName: userName, address: userAddress)
        userHeaderViewModel.send(userHeader)
    }
    
    //MARK: - Fetching user's albums
    fileprivate func fetchUserAlbums(userId: Int) {
        isloading.send(true)
        self.albumApiService.fetchAlbumBasedOnUserID(userID: userId)
            .map([AlbumModel].self)
            .sink { [unowned self] completion in
                switch completion {
                case.failure(let error):
                    self.isloading.send(false)
                    self.alertMessage.send( error.localizedDescription )
                default:
                    break
                }
            } receiveValue: { [unowned self] albums in
                self.albums.send(albums)
                createTableViewVM(albums: albums)
                isloading.send(false)
            }
            .store(in: &cancellable)
    }
    
    //MARK: - generate tableView ViewModel
    fileprivate func createTableViewVM(albums: [AlbumModel]) {
        var albumsVM = [AlbumDetailViewModel]()
        albums.forEach { album in
            guard let albumId = album.id, let albumTitle = album.title else {return}
            albumsVM.append(AlbumDetailViewModel(albumId: albumId, albumTitle: albumTitle))
        }
        albumTableViewModel.send(albumsVM)
    }
    
}
