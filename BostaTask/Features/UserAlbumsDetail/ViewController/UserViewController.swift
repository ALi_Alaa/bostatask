//
//  UserViewController.swift
//  BostaTask
//
//  Created by MacBook Pro on 12/06/2022.
//

import UIKit
import SnapKit
import Combine
import Moya

class UserViewController: UIViewController {

    //MARK: - UIRefereshControl
    let refreshControl = UIRefreshControl()
    
    //MARK: - Properties
    private var loadingState = UIActivityIndicatorView(style: .medium)
    private lazy var errorImageView: UIImageView = {
      let errorImage = UIImageView()
        errorImage.image = UIImage(named: ERROR_IMAGE)
        errorImage.clipsToBounds = true
        errorImage.contentMode = .scaleAspectFill
        return errorImage
    }()
    private lazy var userAlbumTableView = UserAlbumsTableView()
    private lazy var header = HeaderView(frame: CGRect(x: 0, y: 0, width: DEVICE_WIDTH_WITH_SAFE_AREA, height: 55))
    
    var viewModel = UserAlbumsViewModelProvider().viewModelCalling(state: .production)
    
    var cancellable = Set<AnyCancellable>()
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        albumDidTapped()
        // init viewModel status
        initViewModel()
        initHeaderViewModel()
        initTableView()
    }
    
    //MARK: - Selectors
    @objc func refresh(_ sender: AnyObject) {
        ApiConstants.injectRandomNumber(IntNumber: Int.random(in: 1...10))
        viewModel.fetchUser()
        refreshControl.endRefreshing()
    }
    
    //MARK: - ConfigureUI
    private func configureUI() {
        view.backgroundColor = .systemBackground
        self.navigationItem.titleView = header
        userAlbumTableView.albumTableView.refreshControl = refreshControl
        refreshControl.attributedTitle = NSAttributedString(string: APP_NAME)
        refreshControl.tintColor = APP_UNIQUE_COLOR
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        setupTableView()
        setupRefreshControl()
        setupErrorImageView()
    }
    
    /// Setup TableView when state turn to success
    private func setupTableView() {
        view.addSubview(userAlbumTableView)
        userAlbumTableView.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalToSuperview()
        }
    }
    
    /// Setup ActivityIndicator for Loading State
    private func setupRefreshControl() {
        view.addSubview(loadingState)
        loadingState.layer.zPosition = 1
        loadingState.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.height.equalToSuperview()
        }
    }
    
    private func setupErrorImageView() {
        view.addSubview(errorImageView)
        errorImageView.alpha = 0
        errorImageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.height.equalToSuperview()
        }
    }
}

// MARK: - ViewModel Delegate to switch between data State
extension UserViewController {
    func initViewModel() {
        ///Status of network condition
        viewModel.alertMessage
            .sink { [unowned self] message in
                ErrorView.shared.showError(textLabel: message, alertColor: .systemRed, on: self.view)
                self.userAlbumTableView.alpha = 0
                self.errorImageView.alpha = 1
            }.store(in: &cancellable)
        
        viewModel.isloading
            .sink { [unowned self] condition in
                if condition {
                    self.loadingState.startAnimating()
                    self.loadingState.alpha = 1
                    self.errorImageView.alpha = 0
                    self.userAlbumTableView.alpha = 0
                } else {
                    self.loadingState.stopAnimating()
                    self.loadingState.alpha = 0
                    self.userAlbumTableView.alpha = 1
                }
            }.store(in: &cancellable)
    }
    
    func initHeaderViewModel() {
        viewModel.userHeaderViewModel.sink { headerData in
            self.header.setData(viewModel: headerData)
        }.store(in: &cancellable)
    }
    
    func initTableView() {
        viewModel.albumTableViewModel.sink(receiveValue: { [unowned self] albumsVM in
            self.userAlbumTableView.viewModel.send(albumsVM)
        }).store(in: &cancellable)
    }
}

    // MARK: - album Tapped Delegate
extension UserViewController {
    func albumDidTapped() {
        userAlbumTableView.selectedCell.sink { tableCellViewModel in
            let albumDetails = AlbumPhotoDetailsViewController(albumID: tableCellViewModel.albumId, viewTitle: tableCellViewModel.albumTitle)
            self.navigationController?.pushViewController(albumDetails, animated: true)
        }.store(in: &cancellable)
    }
}
