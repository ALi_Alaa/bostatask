//
//  AlbumModel.swift
//  AlbumModel
//
//  Created by MacBook Pro on 14/06/2022.
//

import Foundation

class AlbumModel: Codable {
    
    var userId: Int?
    var id: Int?
    var title: String?
    
    enum CodingKeys: String, CodingKey {
        case userId
        case id
        case title
    }
    
}
