//
//  UserModel.swift
//  UserModel
//
//  Created by MacBook Pro on 13/06/2022.
//

import Foundation

//MARK: - User Model
class UserModel: Codable {
    
    var id: Int?
    var userName: String?
    var address: UserAddress?
    var name: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case userName = "username"
        case address
    }
}

//MARK: - User's Address Model
class UserAddress: Codable {
    var street: String?
    var suite: String?
    var city: String?
    var zipcode: String?
    
    enum CodingKeys: String, CodingKey {
        case street
        case suite
        case city
        case zipcode
    }
    
    var description: CustomStringConvertible {
        guard let street = street, let suite = suite, let city = city, let zipcode = zipcode else {
            return "No address available"
        }
        return "\(street), \(suite), \(city), \(zipcode)"
    }
}
