//
//  UserServices.swift
//  UserServices
//
//  Created by MacBook Pro on 13/06/2022.
//

import Moya
import Foundation

enum UserServices {
    case user
}

extension UserServices: TargetType {
    
    var baseURL: URL {
        switch self {
        case .user:
            guard let url = URL(string: ApiConstants.apiCall(.user)) else { fatalError() }
            return url
        }
    }
    
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        .get
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return nil
    }
    
}
