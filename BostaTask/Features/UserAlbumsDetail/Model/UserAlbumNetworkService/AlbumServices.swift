//
//  AlbumServices.swift
//  AlbumServices
//
//  Created by MacBook Pro on 14/06/2022.
//

import Moya
import Foundation

enum AlbumServices {
    case album(userId: Int)
}

extension AlbumServices: TargetType {
    
    var baseURL: URL {
        switch self {
        case .album(userId: let userId):
            guard let url = URL(string: ApiConstants.apiCall(.albums, id: userId)) else { fatalError() }
            return url
        }
    }
    
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        .get
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return nil
    }
    
}
