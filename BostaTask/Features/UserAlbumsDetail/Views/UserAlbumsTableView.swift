//
//  UserAlbumsTableView.swift
//  UserAlbumsTableView
//
//  Created by MacBook Pro on 13/06/2022.
//

import UIKit
import SnapKit
import Combine

class UserAlbumsTableView: UIView {

    //MARK: - Properties
    var albumTableView = UITableView()
    private let cellId = "cellID"
    
    var viewModel = CurrentValueSubject<[AlbumDetailViewModel]?, Never>([AlbumDetailViewModel]())
    var selectedCell = PassthroughSubject<AlbumDetailViewModel, Never>()
    
    private var cancellable = Set<AnyCancellable>()
    
    //MARK: - Life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    private func initUI() {
        addSubview(albumTableView)
        setupTableView()
        recieveData()
    }
    
    //MARK: - Setup TableView UI and Delegates
    private func setupTableView() {
        albumTableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.bottom.equalTo(self)
            make.left.equalTo(self)
            make.right.equalTo(self)
        }
        albumTableView.dataSource = self
        albumTableView.delegate = self
        albumTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    //MARK: - Reseved Data from View Model Publisher
    func recieveData() {
        viewModel.sink { [unowned self] albums in
            self.albumTableView.reloadData()
        }.store(in: &cancellable)
    }

}


//MARK: - Extension for UITableView Delegates & DataSource
extension UserAlbumsTableView: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = viewModel.value?.count else {
            return 0
        }
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? UITableViewCell else {
            fatalError("could not deque cell")
        }
        guard let model = viewModel.value else {return cell}
        cell.selectionStyle = .none
        cell.textLabel?.text = model[indexPath.row].albumTitle
        return cell

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = viewModel.value else {return}
        selectedCell.send(model[indexPath.row])
    }
}
