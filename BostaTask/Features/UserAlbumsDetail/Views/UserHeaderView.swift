//
//  UserHeaderView.swift
//  UserHeaderView
//
//  Created by MacBook Pro on 13/06/2022.
//

import UIKit
import SnapKit

class HeaderView: UIView {
    
    //MARK: - Properties
    private let userName: UILabel = {
        let userName = UILabel()
        userName.font = PRIMARY_FONT
        userName.textColor = APP_UNIQUE_COLOR
        return userName
    }()
    
    private let userAddress: UILabel = {
        let userAddress = UILabel()
        userAddress.font = SUBTITLE_FONT
        userAddress.textColor = SECONDARY_COLOR
        return userAddress
    }()
    private lazy var stackView = UIStackView(arrangedSubviews: [userName, userAddress])
    
    //MARK: - init
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Life cycle
    override func layoutSubviews() {
        configureUI()
    }
    
    //MARK: - Configure UI
    private func configureUI() {
        stackView.axis = .vertical
        stackView.spacing = UIStackView.spacingUseDefault
        self.addSubview(stackView)
        stackView.snp.makeConstraints({ make in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-10)
        })
    }
    
    /// reseved Data from ViewModel for User Detials
    func setData(viewModel: UserHeaderViewModel) {
        self.userName.text =  viewModel.userName
        self.userAddress.text = viewModel.address
    }
}
