//
//  PhotoModel.swift
//  PhotoModel
//
//  Created by MacBook Pro on 14/06/2022.
//

import Foundation

//MARK: - Photo Model
class AlbumPhotoModel: Codable {
    
    var albumId: Int?
    var id: Int?
    var title: String?
    var url: String?
    var thumbnailUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case albumId
        case id
        case title
        case url
        case thumbnailUrl
    }
}
