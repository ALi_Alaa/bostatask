//
//  PhotosDetailNetworkSource.swift
//  PhotosDetailNetworkSource
//
//  Created by MacBook Pro on 14/06/2022.
//

import Foundation
import CombineMoya
import Moya
import Combine

protocol AlbumPhotosDetailNetworkProtocol {
    func fetchAlbumPhotosBasedOnAlbumID(albumID: Int) -> AnyPublisher<Response, MoyaError>
}

class AlbumPhotosDetailNetworkSource: AlbumPhotosDetailNetworkProtocol {
    
    //MARK: - Moya Provider Properties
    private let provider = MoyaProvider<AlbumPhotosServices>()
    
    //MARK: - Fetch Albums using userID as Param
    func fetchAlbumPhotosBasedOnAlbumID(albumID: Int) -> AnyPublisher<Response, MoyaError> {
        return self.provider.requestPublisher(.photo(albumId: albumID))
    }
    
}
