//
//  PhotoServices.swift
//  PhotoServices
//
//  Created by MacBook Pro on 13/06/2022.
//

import Moya
import Foundation

enum AlbumPhotosServices {
    case photo(albumId: Int)
}

extension AlbumPhotosServices: TargetType {
    
    var baseURL: URL {
        switch self {
        case .photo(albumId: let albumId):
            guard let url = URL(string: ApiConstants.apiCall(.photos, id: albumId)) else { fatalError() }
            return url
        }
    }
    
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        .get
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return nil
    }
    
}
