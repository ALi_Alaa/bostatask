//
//  ImageHeroViewController.swift
//  ImageHeroViewController
//
//  Created by MacBook Pro on 15/06/2022.
//

import UIKit
import Hero
import Kingfisher

class ImageHeroViewController: UIViewController {
    
    //MARK: - Properties
    lazy var imageSelectedHero: UIImageView = {
      let imageHero = UIImageView()
        imageHero.clipsToBounds = true
        imageHero.contentMode = .scaleAspectFill
        return imageHero
    }()
    
    lazy var labelForImageTitle: UILabel = {
      let label = UILabel()
        label.textColor = SECONDARY_COLOR
        label.font = SECONDARY_FONT
        label.textAlignment = .center
        return label
    }()
    
    var imageURL: String!
    var labelText: String!
    
    //MARK: - int with heroImageName & image URL
    init(heroName: String, labelText: String, imageURL: String) {
        super.init(nibName: nil, bundle: nil)
        imageSelectedHero.hero.id = heroName
        self.imageURL = imageURL
        labelForImageTitle.text = labelText
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configurationUI()
        
    }
    
    //MARK: - Configurations
    private func configurationUI() {
        self.hero.isEnabled = true
        view.addSubview(imageSelectedHero)
        view.addSubview(labelForImageTitle)
        imageSelectedHero.isUserInteractionEnabled = true
        imageSelectedHero.kf.setImage(with: URL(string: imageURL),
                                   placeholder: UIImage(imageLiteralResourceName: IMAGE_PLACEHOLDER),
                                   options: [.transition(.fade(1))])
        let userPanned = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        self.imageSelectedHero.addGestureRecognizer(userPanned)
        constraints()
    }
    
    private func constraints() {
        imageSelectedHero.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalToSuperview().inset(60)
            make.height.equalTo(view.snp.width).inset(60)
        }
        labelForImageTitle.snp.makeConstraints { make in
            make.top.equalTo(imageSelectedHero.snp.bottom).offset(12)
            make.left.equalToSuperview().offset(21)
            make.right.equalToSuperview().offset(-21)
        }
    }
    
    //MARK: - Selector
    @objc func handlePan(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: nil)
        let progress = translation.y / 2 / view.bounds.height
        switch sender.state {
        case.began:
            hero.dismissViewController(completion: nil)
        case.changed:
            Hero.shared.update(progress)
            let currentPosition = CGPoint(x: translation.x + imageSelectedHero.center.x, y: translation.y + imageSelectedHero.center.y)
            Hero.shared.apply(modifiers: [.position(currentPosition)], to: imageSelectedHero)
        default:
            if progress > 0.1 {
                Hero.shared.finish()
            }else {
                Hero.shared.cancel()
            }
        }
    }
    
    
}
