//
//  AlbumDetailsViewController.swift
//  AlbumDetailsViewController
//
//  Created by MacBook Pro on 14/06/2022.
//

import UIKit
import SnapKit
import Combine
import Hero

class AlbumPhotoDetailsViewController: UIViewController {
    
    //MARK: - Properties
    private let searchController = UISearchController(searchResultsController: nil)
    
    private var collectionView: UICollectionView?
    
    private var albumId = CurrentValueSubject<Int, Never>(Int())
    
    lazy var viewModel = AlbumPhotosViewModelProvider().viewModelCalling(state: .production, albumId: albumId.value)
    
    lazy var cancellable = Set<AnyCancellable>()
    
    private var LabelForNoData: UILabel = {
      let label = UILabel()
        label.text = NO_DATA_FOUND_MESSAGE
        label.textColor = WARNING_COLOR
        label.font = PRIMARY_FONT
        label.textAlignment = .center
        return label
    }()
    
    //MARK: - init for inject albumID & Life cycle
    init(albumID: Int, viewTitle: String) {
        self.albumId.send(albumID)
        super.init(nibName: nil, bundle: nil)
        title = viewTitle
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupLabelForNoData()
        configureCollectionViewUI()
        constraintsCollectionView()
        setupSearchBarListener()
        searchController.searchBar.delegate = self
        self.view.hero.isEnabled = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        viewModel.availablePhotos.sink { [unowned self] photos in
            if photos?.count == 0 {
                self.LabelForNoData.isHidden = false
            }else {
                self.LabelForNoData.isHidden = true
            }
            self.collectionView?.reloadData()
        }
        .store(in: &cancellable)
    }
    
    //MARK: - Configurations UI
    private func setupNavigationBar() {
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.obscuresBackgroundDuringPresentation = false
    }
    
    /// Setup Label for no data available
    private func setupLabelForNoData() {
        view.addSubview(LabelForNoData)
        LabelForNoData.layer.zPosition = 10
        LabelForNoData.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(55)
            make.center.equalToSuperview()
        }
    }
    
    private func configureCollectionViewUI() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.estimatedItemSize = CGSize(width: (view.frame.size.width/3)-1, height: (view.frame.size.width/3)-4)
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView?.register(AlbumPhotosCollectionViewCell.self, forCellWithReuseIdentifier: AlbumPhotosCollectionViewCell.identifier)
        collectionView?.dataSource = self
        collectionView?.delegate = self
    }
    
    private func constraintsCollectionView() {
        guard let collectionView = collectionView else {return}
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
    
}

//MARK: - delegate when cancel btn Clicked in search bar
extension AlbumPhotoDetailsViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.fetchPhotos()
    }
}

//MARK: - Extension for searchBar Listener
extension AlbumPhotoDetailsViewController {
    fileprivate func setupSearchBarListener() {
        let publisher = NotificationCenter.default.publisher(for: UISearchTextField.textDidChangeNotification, object: searchController.searchBar.searchTextField)
        publisher
            .compactMap {
                ($0.object as? UISearchTextField)?.text
            }
            .sink { [unowned self] searchString in
                self.viewModel.searchText.send(searchString)
            }.store(in: &cancellable)
    }
}
