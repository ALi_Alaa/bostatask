//
//  AlbumPhotosDetail+CollectionView.swift
//  AlbumPhotosDetail+CollectionView
//
//  Created by MacBook Pro on 16/06/2022.
//

import UIKit
import SnapKit
import Combine
import Hero

extension AlbumPhotoDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let photoCount = viewModel.availablePhotos.value?.count {
            return photoCount
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AlbumPhotosCollectionViewCell.identifier, for: indexPath) as! AlbumPhotosCollectionViewCell
        guard let photos = viewModel.availablePhotos.value else {return cell}
        cell.configureCellModel(model: photos[indexPath.row])
        cell.photoAlbum.hero.id = photos[indexPath.row].thumbnailUrl
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = viewModel.availablePhotos.value,
              let thumbnail = cell[indexPath.row].thumbnailUrl,
              let imageTitle = cell[indexPath.row].title else {
                  return
              }
        let imageViewContainer = ImageHeroViewController(heroName: thumbnail, labelText: imageTitle, imageURL: thumbnail)
        imageViewContainer.view.hero.modifiers = [.opacity(0)]
        imageViewContainer.heroModalAnimationType = .zoom
        self.navigationController?.present(imageViewContainer, animated: true, completion: nil)
    }
}
