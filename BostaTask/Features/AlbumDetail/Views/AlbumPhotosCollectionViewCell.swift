//
//  AlbumPicturesCollectionViewCell.swift
//  AlbumPicturesCollectionViewCell
//
//  Created by MacBook Pro on 14/06/2022.
//

import UIKit
import Kingfisher

class AlbumPhotosCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Properties
    static let identifier = "AlbumPhotoCollectionViewCell"
    
    // Constants for View
    private var CELL_WIDTH: CGFloat {return contentView.frame.size.width}
    private var CELL_HEIGHT: CGFloat {return contentView.frame.size.height}
    
    lazy var photoAlbum: UIImageView = {
      let photoAlbum = UIImageView()
        photoAlbum.contentMode = .scaleAspectFill
        photoAlbum.clipsToBounds = true
        photoAlbum.backgroundColor = .label
        return photoAlbum
    }()
    
    //MARK: - Life cycle
    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
    
    //MARK: - ConfigureUI
    fileprivate func configureUI() {
        contentView.addSubview(photoAlbum)
        photoAlbum.frame = CGRect(x: 0, y: 0, width: CELL_WIDTH, height: CELL_HEIGHT)
    }
    
    //MARK: - Configure Cell
    func configureCellModel(model: AlbumPhotoModel) {
        guard let thumbnailURL = model.thumbnailUrl else {return}        
        photoAlbum.kf.indicatorType = .activity
        photoAlbum.kf.setImage(with: URL(string: thumbnailURL),
                               placeholder: UIImage(imageLiteralResourceName: IMAGE_PLACEHOLDER),
                               options: [.transition(.fade(1))])
    }
}
