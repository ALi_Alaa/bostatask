//
//  AlbumPhotosViewModelProvider.swift
//  AlbumPhotosViewModelProvider
//
//  Created by MacBook Pro on 15/06/2022.
//

import Foundation

//MARK: - Production State
enum AlbumPhotosNetworkingState {
    case production
}

//MARK: - User's Album ViewModel Provider
class AlbumPhotosViewModelProvider {
    
    //MARK: - View Model Provider function for upcomming networking changes
    func viewModelCalling(state: AlbumPhotosNetworkingState, albumId: Int) -> AlbumPhotosViewModel  {
        switch state {
        case .production:
            return AlbumPhotosViewModel(API: AlbumPhotosDetailNetworkSource(), albumId: albumId)
        }
    }
}

