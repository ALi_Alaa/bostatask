//
//  AlbumPhotosViewModel.swift
//  AlbumPhotosViewModel
//
//  Created by MacBook Pro on 15/06/2022.
//

import Combine
import Foundation


class AlbumPhotosViewModel {
    
    //MARK: - Properties
    private let albumPhotoService: AlbumPhotosDetailNetworkProtocol

    private var cancellable = Set<AnyCancellable>()
    var availablePhotos = CurrentValueSubject<[AlbumPhotoModel]?, Never>([AlbumPhotoModel]())
    fileprivate var albumId = CurrentValueSubject<Int, Never>(Int())
    var searchText = PassthroughSubject<String, Never>()
    lazy var alertMessage = PassthroughSubject<String, Never>()
    
    fileprivate var fullAlbum = [AlbumPhotoModel]()
    
    //MARK: - init to inject the depenedency
    init(API: AlbumPhotosDetailNetworkProtocol, albumId: Int) {
        self.albumPhotoService = API
        self.albumId.send(albumId)
        fetchPhotos()
    }
    
    //MARK: - Helpers
    /// Fetching photo for init
    func fetchPhotos() {
        albumPhotoService.fetchAlbumPhotosBasedOnAlbumID(albumID: albumId.value)
            .map([AlbumPhotoModel].self)
            .sink { [unowned self] response in
                switch response {
                case .failure(let error):
                    self.alertMessage.send(error.localizedDescription)
                default:
                    break
                }
            } receiveValue: { [unowned self] data in
                self.availablePhotos.send(data)
                self.fullAlbum = data
                fetchDataForSearchBarText()
            }
            .store(in: &cancellable)
    }
    
    //MARK: - Fetching Search Text
    func fetchDataForSearchBarText() {
        searchText
            .removeDuplicates()
            .debounce(for: .milliseconds(500), scheduler: RunLoop.main)
            .map { [unowned self] text in
                guard !self.fullAlbum.isEmpty else {return}
                var availablePhotos = [AlbumPhotoModel]()
                for album in self.fullAlbum {
                    let range = album.title?.lowercased().range(of: text, options: .caseInsensitive)
                    if range != nil {
                        availablePhotos.append(album)
                    }
                }
                if text.isEmpty {
                    self.availablePhotos.send(self.fullAlbum)
                } else {
                    self.availablePhotos.send(availablePhotos)
                }
            }
            .sink { _ in }.store(in: &cancellable)
    }
    
}
