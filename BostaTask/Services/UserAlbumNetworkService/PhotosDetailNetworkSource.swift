//
//  PhotosDetailNetworkSource.swift
//  PhotosDetailNetworkSource
//
//  Created by MacBook Pro on 15/06/2022.
//

import CombineMoya
import Moya
import Combine

protocol PhotoDetailNetworkProtocol {
    func fetchAlbumPhotosBasedOnAlbumID(albumID: Int) -> AnyPublisher<Response, MoyaError>
}

class PhotosDetailNetworkSource: PhotoDetailNetworkProtocol {
    
    //MARK: - Moya Provider Properties
    private let provider = MoyaProvider<PhotoServices>()
    
    //MARK: - Fetch Albums using userID as Param
    func fetchAlbumPhotosBasedOnAlbumID(albumID: Int) -> AnyPublisher<Response, MoyaError> {
        return self.provider.requestPublisher(.photo(albumId: albumID))
    }
    
}

