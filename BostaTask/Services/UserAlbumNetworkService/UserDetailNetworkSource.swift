//
//  UserNetwork.swift
//  UserNetwork
//
//  Created by MacBook Pro on 13/06/2022.
//

import Foundation
import CombineMoya
import Moya
import Combine


protocol UserDetailNetworkProtocol {
    func fetchUser() -> AnyPublisher<Response, MoyaError>
}

class UserDetailNetworkSource: UserDetailNetworkProtocol {

    //MARK: - Moya Provider Properties
    private let provider = MoyaProvider<UserServices>()
    
    var cancallable = Set<AnyCancellable>()
    
    //MARK: - Fetching single User
    func fetchUser() -> AnyPublisher<Response, MoyaError> {
        return self.provider.requestPublisher(.user)
    }
}
