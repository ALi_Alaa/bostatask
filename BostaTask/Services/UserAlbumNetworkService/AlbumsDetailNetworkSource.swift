//
//  AlbumsNetwork.swift
//  AlbumsNetwork
//
//  Created by MacBook Pro on 14/06/2022.
//

import CombineMoya
import Moya
import Combine

protocol AlbumDetailNetworkProtocol {
    func fetchAlbumBasedOnUserID(userID: Int) -> AnyPublisher<Response, MoyaError>
}

class AlbumsDetailNetworkSource: AlbumDetailNetworkProtocol {
    
    //MARK: - Moya Provider Properties
    private let provider = MoyaProvider<AlbumServices>()
    
    //MARK: - Fetch Albums using userID as Param
    func fetchAlbumBasedOnUserID(userID: Int) -> AnyPublisher<Response, MoyaError> {
        return self.provider.requestPublisher(.album(userId: userID))
    }    
    
}
