//
//  PhotoServices.swift
//  PhotoServices
//
//  Created by MacBook Pro on 15/06/2022.
//

import Moya
import Foundation

enum PhotoServices {
    case photo(albumId: Int)
}

extension PhotoServices: TargetType {
    
    var baseURL: URL {
        switch self {
        case .photo(albumId: let albumId):
            guard let url = URL(string: ApiConstants.apiCall(.photos, id: albumId)) else { fatalError() }
            return url
        }
    }
    
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        .get
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
}
