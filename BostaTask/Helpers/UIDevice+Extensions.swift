//
//  UIDevice+Extesion.swift
//  UIDevice+Extesion
//
//  Created by MacBook Pro on 14/06/2022.
//

import UIKit

extension UIDevice {
    var notch: CGFloat {
        let top = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
        return top
    }
}
