//
//  Constants.swift
//  Constants
//
//  Created by MacBook Pro on 14/06/2022.
//

import UIKit

let APP_NAME = "Pull for Bosta"

//MARK: - Constraints FOR UI
let DEVICE_WIDTH = UIScreen.main.bounds.size.width
let DEVICE_WIDTH_WITH_SAFE_AREA = DEVICE_WIDTH - 60

let DEFAULT_SAVE_BOTTOM_SPACE = -UIDevice().notch

//MARK: - Fonts Size
let SUBTITLE_FONT = UIFont.systemFont(ofSize: 12, weight: .light)
let SECONDARY_FONT = UIFont.systemFont(ofSize: 14, weight: .bold)
let PRIMARY_FONT = UIFont.systemFont(ofSize: 16, weight: .bold)

//MARK: - Color Schema
let APP_UNIQUE_COLOR = UIColor.systemRed
let SECONDARY_COLOR = UIColor.systemGray2
let WARNING_COLOR = UIColor.systemRed

//MARK: - Animation timing referance
let DELAY: DispatchTime = .now() + 5
let DURATION_ANIMATION = 0.6

// MARK: - Image Reference's Name
let ERROR_IMAGE = "Error_Image"
let IMAGE_PLACEHOLDER = "ImagePlaceholder"

// MARK: - Messages
let NO_DATA_FOUND_MESSAGE = "No Data found for this search..."
let FAILURE_IN_GETING_USER_DATA = "Error In Getting User Data"
