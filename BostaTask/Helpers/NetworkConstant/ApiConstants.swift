//
//  Constants.swift
//  Constants
//
//  Created by MacBook Pro on 13/06/2022.
//

import Foundation

class ApiConstants {
    
    // MARK: - filePrivate class baseApi
    fileprivate class API {
        static var baseUrl = "https://jsonplaceholder.typicode.com"
        static var randomUserId = Int.random(in: 1...10)
    }
    
    /// Inject random Number for refreshing the user ID and so it will update user album too
    static func injectRandomNumber(IntNumber: Int) {
        ApiConstants.API.randomUserId = IntNumber
    }
    
    // MARK: - Function to call endPoint
    /// By Default it will generate random number for user ID as the init for the APP
    static func apiCall(_ endPoint: endPoint, id: Int = ApiConstants.API.randomUserId) -> String {
         return ApiConstants.API.baseUrl + endPoint.rawValue + "\(id)"
    }
    
}

// MARK: - endPoint for URL
enum endPoint: String {
    case user = "/users/"
    case albums = "/albums?userId="
    case photos = "/photos?albumId="
}
