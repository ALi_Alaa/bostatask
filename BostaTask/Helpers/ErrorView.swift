//
//  ErrorView.swift
//  ErrorView
//
//  Created by MacBook Pro on 14/06/2022.
//

import UIKit
import SnapKit

class ErrorView {
    
    // MARK: - Properties
    static let shared = ErrorView()
    
    private let backGroundView: UIView = {
    let view = UIView()
        view.layer.cornerRadius = 12
        view.clipsToBounds = true
        return view
    }()
    
    private let imageView: UIImageView = {
      let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.tintColor = .darkGray
        return iv
    }()
    
    private let label: UILabel = {
      let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = .white
        lbl.textAlignment = .left
        return lbl
    }()
    
    private lazy var stack = UIStackView(arrangedSubviews: [imageView, label])
    
    //MARK: - Private init
    private init() { }
    
    // MARK: - Helpers
    fileprivate func configureUI(textLabel: String, alertColor: UIColor, on view: UIView, image: String?) {
        view.addSubview(backGroundView)
        backGroundView.backgroundColor = alertColor
        backGroundView.layer.opacity = 0
        backGroundView.layer.zPosition = 10
        backGroundView.bringSubviewToFront(view)
        backGroundView.addSubview(stack)
        stack.spacing = 8
        stack.distribution = .fillProportionally
        guard let imageString = image else {
                return
        }
        imageView.image = UIImage(systemName: imageString)
        imageView.tintColor = .white
        label.text = textLabel
    }
    
    fileprivate func viewsConstaints() {
        backGroundView.snp.makeConstraints { (maker) in
            maker.width.equalToSuperview().inset(15)
            maker.height.equalTo(60)
            maker.bottom.equalToSuperview()
            maker.centerX.equalToSuperview()
        }
        imageView.snp.makeConstraints { (maker) in
            maker.width.height.equalTo(24)
        }
        stack.snp.makeConstraints { (maker) in
            maker.center.equalToSuperview()
            maker.width.equalToSuperview().inset(15)
        }
    }
    
    fileprivate func animation() {
        UIView.animate(withDuration: DURATION_ANIMATION) {
            self.backGroundView.transform = CGAffineTransform(translationX: 0, y: DEFAULT_SAVE_BOTTOM_SPACE)
            self.backGroundView.layer.opacity = 1
        }
        DispatchQueue.main.asyncAfter(deadline: DELAY) {
            self.dismiss()
        }
    }
    
}

//MARK: - Client Using Funcation
extension ErrorView {
    /// Show error view
    func showError(textLabel: String, alertColor: UIColor, image: String? = "network", on view: UIView) {
        configureUI(textLabel: textLabel, alertColor: alertColor, on: view, image: image)
        viewsConstaints()
        animation()
    }
    
    /// Dismiss error view
    func dismiss() {
        UIView.animate(withDuration: DURATION_ANIMATION) {
            self.backGroundView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.backGroundView.layer.opacity = 0
            self.backGroundView.removeFromSuperview()
        }
    }
}
